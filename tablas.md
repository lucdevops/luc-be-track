**Documentación de tablas**

**Tabla Usuarios**

| Campo | Descripción | Replicado | Tipo de dato | Llave primaría |Auto incrementar |Nulo |Llave foránea Tabla//Campo |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| idUsuario | Documento que agrupa órdenes de compra de una ruta | SI | CHAR(20) | SI | NO | NO | NO |
| ID | Cédula dil usuario | SI | CHAR(20) | SI | NO | NO | NO |
| nombres | Nombres del usuario | SI | CHAR(20) | NO | NO | NO | NO |
| Apellidos | Apellidos del usuario| SI | CHAR(20) | NO | NO | NO | NO |
| Correo | Correo electrónico del usuario| SI | CHAR(20) | NO | NO | SI | NO |
| roll| Dependiendo del Roll tendremos diferentes permisos en el aplicativo. Posibles: Coordinador Logístico, Repartidor, Ayudante, Proveedor. | NO | CHAR(20) | NO | NO | NO | NO |

**Tabla planillas**

| Campo | Descripción | Replicado | Tipo de dato | Llave primaría |Auto incrementar |Nulo |Llave foránea Tabla//Campo |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| planilla  | Documento que agrupa órdenes de compra de una ruta | SI  | CHAR | SI | NO | NO  | NO |
| nombreRuta    | Nombre de la ruta física que realiza el conductor  | SI                                                              | CHAR | NO | NO | NO  | NO          |
| fechaCreacion | Fecha de creación de la planilla                   | SI                                                              | DATE | SI | NO | NO  | NO          |
| fechaDespacho | Facha de despacho de la planilla                   | SI                                                              | DATE | SI | NO | NO  | NO          |
| placa   | Identificación de vehículo que realizará despacho. | SI                                                              | CHAR | NO | NO | NO  | SI          |
| repartidor    | Usuario de la app administra las entregas          | NO, lo debe indicar el coordinador logístico.                   | CHAR | NO | NO | NO  | Usuarios||user |
| ayudante1     | Usuario de la app que colabora al repartidor       | NO, lo debe indicar el coordinador logístico.                   | CHAR | NO | NO | SI  | Usuarios||idUsuario |
| ayudante2     | Usuario de la app que colabora al repartidor       | NO lo debe indicar el coordinador logístico.                    | CHAR | NO | NO | SI  | Usuarios||idUsuario |
| cantidadCajas | Cantidad de cajas que se entregarán en la planilla | NO, hasta el momento lo debe ingresar el coordinador logístico. | INT  | NO | NO | SI  | NO          |

**Tabla OrdenesDeCompra**

| Campo            |   Descripción                                                                                                        |   Replicado   |   Tipo de dato   |   Llave primaría   |  Auto incrementar   |  Nulo      |  Llave foránea Tabla//Campo                                 |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
|   planilla       |   Documento que agrupa órdenes de compra de una ruta                                                                 |   SI          |   CHAR(20)       |   SI               |   NO                |   NO       |   planillas//planilla                                       |
|   ordencompra    |   Documento que agrupa los pedidos por proveedor para cada cliente                                                   |   SI          |   CHAR(20)       |  SI                |   NO                |   NO       |  NO                                                         |
|   codProveedor   |   Empresa a la cual se hace representación de sus productos                                                          |   SI          |   CHAR(20)       |  SI                |   NO                |   NO       |  proveedores//codProveedor                                  |
|   codCliente     |   Código de cliente a entregar orden de compra                                                                       |   SI          |   CHAR(20)       |  SI                |   NO                |   NO       |  clientes/codCliente                                        |
|   repartidor     |   Persona que administra la entrega de los productos en el despacho de los productos. Puede ser los ayudantes        |   SI          |   CHAR(20)       |  NO                |   NO                |  SI        |  Puede ser el repartidor principal, ayudante1 o ayudante 2  |
|   fact           |   Factura asociada a la orden de compra                                                                              |   SI          |   CHAR(20)       |  NO                |   NO                |  NO        |  NO                                                         |
|   numCaja        |   Número de la caja en la cual se encuentra almacenado el producto asociado a la orden de compra.                    |   SI          |   INT            |  NO                |   NO                |  SI        |  NO                                                         |
|   estado         |   Estaba en la cual se encuentra el pedido. Posibles: En tránsito, entregado, entregado con novedad, no entregado.   |   NO          |   CHAR(20)       |  NO                |   NO                |  NO        |  NO                                                         |
|   timeStamp      |   Hora y fecha de la ultima etapa registrada                                                                         |   NO          |   DATE           |  SI                |   NO                |  SI        |  NO                                                         |
|   lat            |   Latitud registrada al momento de almacenar el ultimo estado del pedido.                                            |   NO          |   CHAT           |  NO                |   NO                |  SI        |  NO                                                         |
|   lon            |   Longitud registrada al momento de almacenar el ultimo estado del pedido.                                           |   NO          |   CHAR           |  NO                |   NO                |  SI        |  NO                                                         |
|   efectivo       |   Se almacena el si el cliente entrego en efectivo  el dinero de la orden de compra.                                 |   NO          |   BOOL           |  NO                |   NO                |  SI        |  NO                                                         |

**Tabla detOrdenesDeCompra**

| Campo        |  Descripción                                                       |  Replicado  |  Tipo de dato  |  Llave primaría  | Auto incrementar  | Nulo     | Llave foránea Tabla//Campo   |
|  ------      |  ------                                                            |  ------     |  ------        |  ------          |  ------           |  ------  |  ------                      |
| ordenCompra  |  Documento que agrupa los pedidos por proveedor para cada cliente  | SI          |  CHAR(20)      | SI               | NO                | NO       | OrdenesDeCompra//ordenCompra |
| secuencia    | Numeración de los ítems del pedido                                 | NO          | INT            | SI               | SI                | NO       | NO                           |
| codArticulo  | Código de referencia del artículo                                  | SI          |  CHAR(20)      | SI               | NO                | NO       | articulos//codArticulo       |
| cantidad     | Numero de unidades del articulo solicitadas por el cliente         | SI          | INT            | NO               | NO                | NO       | NO                           |
| unidad       | Unidad de medida con que se pidió el articulo                      | SI          | CHAR(20)       | NO               | NO                | NO       | NO                           |
| unidadConv   | Factor de conversión al momento de capturar la orden de compra     | SI          | DECIMAL        | NO               | NO                | NO       | NO                           |
| precio       | Valor comercial por unidad del artículo.                           | SI          | DECIMAL        | NO               | NO                | NO       | NO                           |

**Tabla seguimiento**

| Campo     |  Descripción                                                                |  Replicado  |  Tipo de dato  |  Llave primaría  | Auto incrementar  | Nulo     | Llave foránea Tabla//Campo  |
|  ------   |  ------                                                                     |  ------     |  ------        |  ------          |  ------           |  ------  |  ------                     |
| planilla  |  Documento que agrupa órdenes de compra de una ruta                         | si          |  CHAR(20)      | SI               | NO                | NO       |  planillas//planilla        |
| lat       |  Latitud registrada al momento de almacenar el ultimo estado del pedido.    | no          |  CHAR(20)         | NO               | NO                | NO       | NO                          |
| lon       |  Longitud registrada al momento de almacenar el ultimo estado del pedido.   | no          |  CHAR(20)          | NO               | NO                | NO       | NO                          |
| timeStamp |  Hora y fecha del momento                                                   | no          | DATE           | NO               | NO                | NO       | NO                          |


**Tabla artículos** 

|  Campo             |  Descripción                       |  Replicado  |  Tipo de dato  |  Llave primaría  | Auto incrementar  | Nulo     | Llave foránea Tabla//Campo   |
|  ------            |  ------                            |  ------     |  ------        |  ------          |  ------           |  ------  |  ------                      |
| CodArticulo        | Código del artítulo                | SI          |  CHAR(20)      | SI               | NO                | NO       | NO                           |
| descripcion        | Descripción del artículo           | SI          |  CHAR(20)      | NO               | NO                | NO       | NO                           |
|  CodProveedor      | Proveedor que lo distribulle       | SI          |  CHAR(20)      | SI               | NO                | NO       | proveedores//idProveedor     |

**tabla clientes**      
    
|  Campo             |  Descripción                       |  Replicado  |  Tipo de dato  |  Llave primaría  | Auto incrementar  | Nulo     | Llave foránea Tabla//Campo   |
|  ------            |  ------                            |  ------     |  ------        |  ------          |  ------           |  ------  |  ------                      |
| codCliente         |                                    | SI          |  CHAR(20)      | SI               | NO                | NO       | NO                           |
| id                 |                                    | SI          |  CHAR(20)      | SI               | NO                | NO       | NO                           |
| nombre             |                                    | SI          |  CHAR(20)      | NO               | NO                | NO       | NO                           |
| direccion          |                                    | SI          |  CHAR(20)      | NO               | NO                | NO       | NO                           |
| lat                |                                    | NO          |  CHAR(20)      | NO               | NO                | SI       | NO                           |
| lon                |                                    | NO          |  CHAR(20)      | NO               | NO                | SI       | NO                           |

**Tabla proveedores**

|  Campo             |  Descripción                       |  Replicado  |  Tipo de dato  |  Llave primaría  | Auto incrementar  | Nulo     | Llave foránea Tabla//Campo   |
|  ------            |  ------                            |  ------     |  ------        |  ------          |  ------           |  ------  |  ------                      |
| CodProveedor       |                                    | SI          |  CHAR(20)      | SI               | NO                | NO       | NO                           |
|  ID                |                                    | SI          |  CHAR(20)      | SI               | NO                | NO       | NO                           |
| nombre             |                                    | SI          |  CHAR(20)      | NO               | NO                | NO       | NO                           |
| direcion           |                                    | SI          |  CHAR(20)      | NO               | NO                | NO       | NO                           |

**Tabla historicoNovedades**

|  Campo             |  Descripción                       |  Replicado  |  Tipo de dato  |  Llave primaría  | Auto incrementar  | Nulo     | Llave foránea Tabla//Campo   |
|  ------            |  ------                            |  ------     |  ------        |  ------          |  ------           |  ------  |  ------                      |
| ordenCompra        |                                    | SI          |  CHAR(20)      | SI               | NO                | NO       | OrdenesDeCompra//ordenCompra |
| secuencia          |                                    | NO          |  CHAR(20)      | SI               | NO                | NO       | OrdenesDeCompra//secuencia   |
| lat                |                                    | NO          |  CHAR(20)      | NO               | NO                | NO       | NO                           |
| lon                |                                    | NO          |  CHAR(20)      | NO               | NO                | NO       | NO                           |
|  codNovedad        |                                    | NO          |  CHAR(20)      | NO               | NO                | NO       | novedades//codNovedad        |

**Tabla novedades**    

|  Campo             |  Descripción                       |  Replicado  |  Tipo de dato  |  Llave primaría  | Auto incrementar  | Nulo     | Llave foránea Tabla//Campo   |
|  ------            |  ------                            |  ------     |  ------        |  ------          |  ------           |  ------  |  ------                      |
| codNovedad         | Identificación de la novedad       | NO          |  CHAR(20)      | SI               | NO                | NO       | NO                           |
| descripción        | Detalle explicativo de la novedad  | NO          |  CHAR(40)      | NO               | NO                | NO       | NO                           |